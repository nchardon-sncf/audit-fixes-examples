# Accessibility fixes

This project contains suggested solutions for problems encountered during accessibility audits. 
Examples only use CSS, HTML and Javascript. 

# Corrections pour l'accessibilité 

Ce projet rassemble les solutions suggérées aux problèmes détectés durant les audits d'accessibilité. 
Ces examples n'utilisent que CSS, HTML et Javascript. 
